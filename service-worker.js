const pwaCacheVersion = 'esgieat-static-v1';
const assets = [
		'/',
		'/index.html',
		'/restaurants',
		'/menus',
		'/order',
		'/signIn',
		'/signUp',
		'/app.js',
		'/service-worker.js',
		'/assets/styles.css',
		'/assets/images',
		'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.1/js/all.min.js',
		"https://cdnjs.cloudflare.com/ajax/libs/firebase/7.16.0/firebase-app.min.js",
		"https://cdnjs.cloudflare.com/ajax/libs/firebase/7.16.0/firebase-auth.min.js",
]

// Listen the Install Event On The ServiceWorker
self.addEventListener('install', event => {
		event.waitUntil(
				caches.open(pwaCacheVersion).then(cache => {
						console.log('caching shell assets');
						cache.addAll(assets)
								.then(() => self.skipWaiting());
				})
		)
});

// Listen The Activate Event On The serviceWorker
self.addEventListener('activate', evt => {
		evt.waitUntil(
				caches.keys().then(keys => {
						//console.log(keys);
						return Promise.all(keys
								.filter(key => key !== pwaCacheVersion)
								.map(key => caches.delete(key))
						);
				})
		);
})

// CheckConnection
/*self.addEventListener('fetch', event => {
		if (!navigator.online) {
				const headers = {headers: {'Content-Type': 'text/html'}};
				event.respondWith(new Response(`<h1>No internet connection.Try again later</h1v>`))
		}
});*/

self.addEventListener('fetch', event => {
		event.respondWith(
				caches.match(event.request).then(cacheRes => {
						return cacheRes || fetch(event.request)
				})
		);
});

function grantNotification() {
		if (('serviceWorker' in navigator) || 'PushManager' in window) {
				Notification.requestPermission()
						.then(result => {
								if (result === 'denied') {
										console.log("Permission not granted");
								} else if (result === 'default') {
										console.log('Permission was dismissed');
								}

								console.log('Notification granted', result);
								subcribeNotification();
						})
		} else {
				console.log("Sorry, Web push notification aren't supported on your device :'(");
		}
}
