// import page from 'page';
import { html, render } from 'lit-html';
// import '../components/restaurant-store.js';


export default class Order {
	constructor(page) {
		this.page = page;
		this.properties = {
			cartItem: JSON.parse(localStorage.getItem('cart')),
			bool: true
		};
		if(this.properties.cartItem === null){
			this.properties.bool = false
		}
		this.renderView(this.properties.restaurant);
	}

	template() {
		return html`
				<h1 class="text-gray-900 font-bold text-xl justify-center uppercase">récapitulatif de votre Commande</h1>
				<div>
				${this.properties.bool ? this.properties.cartItem.map(item => html`
					<div class="bg-white border overflow-hidden rounded-lg mt-2">
						<div class="px-4 py-2">
							<h3 class="font-bold text-sm mt-1">${item.name}</h3>
						</div>
						<img src="${item.img}" alt="img_product">
						<div class="flex items-center justify-between px-4 py-2 bg-yellow-600">
							<h1 class="text-gray-200 font-bold text-xl">${item.price}€</h1>
							<button type="button" class="px-3 py-1 bg-gray-200 text-sm text-gray-900 font-semibold rounded" @click="${this.validCommand}">Valider votre commande</button>
						</div>
					</div>

					`) : html`
					<h3>Panier vide. Veuillez ajouter un article</h3>	`}
				</div>

				`
	}

	renderView() {
		const view = this.template();
		console.log('viewwwwww1', view)
		render(view, this.page);
	}

	validCommand(){
		localStorage.removeItem("cart", null);

		window.location.href('/restaurants')
	}
}
