import page from 'page';
import { html, render } from 'lit-html';
import '../components/restaurant-store.js';

export default class Restaurant {
  constructor(page) {
    this.page = page;

    const mutationObserver = new MutationObserver(this.observeAttribute.bind(this));
    mutationObserver.observe(this.page, { attributes: true });

    this.properties = {
      restaurant: '',
      restaurants: []
    };

    this.renderView(this.properties.restaurants);
  }

  set restaurants(value) {
    this.properties.restaurants = value;
  }

  get restaurants() {
    return this.properties.restaurants;
  }

  observeAttribute(mutationsList, observer) {
    mutationsList.forEach(mutation => {
      if (mutation.type === 'attributes') {
        switch(mutation.attributeName) {
          case 'active':
            if (this.page.getAttribute('active') === 'true') {
              const store = this.page.querySelector('restaurant-store');
              store.activated();
              observer.disconnect();
            }
            break;
        }
      }
    });
  }

  template() {
    return html`
          <restaurant-store collection="restaurants" @data-changed="${this.handleChange.bind(this)}"></restaurant-store>
          <section>
            <main>
              <div class="h-64 w-screen flex-1 text-center text-white -mt-2 font-extrabold bg-yellow-600 flex justify-center items-center flex-wrap">
                <div class="title-restaurant my-12 text-xl">Vos Restaurants préférés, livrés chez vous en un clic!</div>
                <div>
                <form @submit="${this.handleForm.bind(this)}" id="idRestaurant" class="w-full h-full flex justify-between items-center px-4 py-3 mb-8">
                    <label class="flex-1 aria-label="Recherchez un restaurant">
                    <input
                      autocomplete="off"
                      .value="${this.properties.restaurant}"
                      @input="${e => this.properties.restaurant = e.target.value}"
                      class="py-3 px-4 rounded-sm w-full h-full text-black"
                      type="text"
                      placeholder="Recherchez un restaurant ..."
                      name="restaurant">
                    </label>
                    <button
                      aria-label="Recherchez"
                      class="py-2 rounded-sm w-24 text-uppercase bg-green-800 text-center "
                      type="submit">
                      Recherchez
                    </button>
                  </form>
                </div>

              </div>
              <ul>
                ${this.properties.restaurants.map(restaurant => html`
                <li class="container container-resto" id="${restaurant.name.toLowerCase().replace(' ', '')}">
                <button class="title-resto text-center font-serif font-extrabold" data-id=${restaurant.id} attribut @click="${this.switchPage}">
                ${restaurant.name}
                </button>
                <img src=${restaurant.img} alt="img-restau">
                </li>
              `)}
              </ul>
            </main>
          </section>
        `
  }

  renderView() {
    const view = this.template();
    render(view, this.page);
  }

  handleChange({ detail: data }) {
    this.properties.restaurants = data;
    this.renderView();
  }

  switchPage(e) {
    e.preventDefault();
    const idRestautant = e.target.getAttribute('data-id');
    page('/restaurant/' + idRestautant)
  }

  handleForm(e) {
    e.preventDefault();
    const li = document.getElementsByTagName('li');
    for(let i = 0; i < li.length; i++){
      const item = li[i];
      if(!item.id.includes(this.properties.restaurant.toLowerCase().replace(' ', ''))){
        item.hidden = true;
      }
    }

    const input = document.querySelector('[name="restaurant"]');
    input.value = '';
    this.properties.restaurant = '';
  }
}





