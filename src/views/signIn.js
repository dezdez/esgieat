import { html, render } from "lit-html";

export default class SignIn {
  constructor(page) {
    this.page = page;
    this.properties = {
      email: '',
      password: '',
    };

    this.renderView();
  }

  template() {
    return html`

    <div style="height: 500px">
      <div class="fixed pin flex items-center">
        <div class="fixed pin bg-black opacity-75 z-10"></div>
        <div class="relative mx-6 md:mx-auto w-full md:w-1/2 lg:w-1/3 z-20 m-8">
          <div class="shadow-lg bg-white rounded-lg p-8">
            <div class="flex justify-end mb-6">
            </div>
            <h1 class="text-center text-2xl text-green-dark">Login</h1>
            <form class="pt-6 pb-2 my-2" @submit="${this.handleForm.bind(this)}">
              <div class="mb-4">
                <label class="block text-sm font-bold mb-2" for="email">
                  Email Address
                </label>
                <input class="shadow appearance-none border rounded w-full py-2 px-3 text-grey-darker" type="email" name="email" id="email" placeholder="Email Address"
                @input="${e => this.properties.email = e.target.value}">
              </div>
              <div class="mb-6">
                <label class="block text-sm font-bold mb-2" for="password">
                  Password
                </label>
                <input class="shadow appearance-none border rounded w-full py-2 px-3 text-grey-darker mb-3" type="password" name="password" id="password" placeholder="Password"
                @input="${e => this.properties.password = e.target.value}">
              </div>
              <div class="block md:flex items-center justify-between">
                <div>
                  <button class="bg-yellow-600 hover:bg-green-dark text-white font-bold py-2 px-4 rounded border-b-4 border-green-darkest" type="submit">
                    Sign In
                  </button>
                </div>
                <div class="mt-4 md:mt-0">
                  <a href="#" class="text-green no-underline">Forget Password?</a>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
    `
  }

  renderView() {
    const view = this.template();
    render(view, this.page);
  }

  handleForm(e) {
    e.preventDefault();
    const event = new CustomEvent('sing-in', {
      detail: {
        email: this.properties.email,
        password: this.properties.password,
        imgprofile: this.properties.file,
      }
    });
    document.dispatchEvent(event);
    const email = document.querySelector('[name="email"]');
    const password = document.querySelector('[name="password"]');
    email.value = '';
    password.value = '';
  }
}
