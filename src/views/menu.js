import { html, render } from 'lit-html';

export default class Menu {
    constructor(page, ctx) {
        this.page = page;
        this.ctx = ctx;

        this.properties = {
            newCommentary: '',
            newCommentaryNote: 0,
            restaurant: {
                id: this.ctx.pathname.split('/').pop(),
                menus: [],
                commentaries: []
            }
        };


        console.log('menu properties', this.properties)

        this.renderView(this.properties.restaurants);
    }

    set restaurants(value) {
        this.properties.restaurants = value;
    }

    get restaurants() {
        return this.properties.restaurants;
    }

    template() {
        return html`
				<restaurant-store document=${this.properties.restaurant.id} @data-restaurant="${this.handleChange.bind(this)}"></restaurant-store>
                <section>
                    <main>
                        <h1 class="text-teal-900 text-center font-bold text-lg ">${this.properties.restaurant.name}</h1>
                    </main>
                        <div class="h-64 w-screen flex-1 text-center text-white font-extrabold bg-yellow-600 flex justify-center items-center flex-wrap overflow-auto">
                            <div class="pl-6 pr-6">Les avis des clients</div>
                            <form @submit="${this.handleForm.bind(this)}" id="idRestaurant" class="w-full h-60 flex justify-between items-center px-4 py-3">
                            <label class="flex-1 aria-label="Votre commentaire">
                                <input
                                    autocomplete="off"
                                    .value="${this.properties.newCommentary}"
                                    @input="${e => this.properties.newCommentary = e.target.value}"
                                    class="py-3 px-4 rounded-sm w-full h-full text-black"
                                    type="text"
                                    placeholder="Votre commentaire ..."
                                    name="commentaire">
                                </label>
                            <button
                                aria-label="Envoyer"
                                class="py-2 rounded-sm w-24 text-uppercase bg-green-800 text-center "
                                type="submit">
                                Envoyer
                            </button>
                            </form>
                            <div class="p-6">
                                ${this.properties.restaurant.commentaries.map(commentary => html`


                                <p class="flex items-baseline">
                                <span class="text-gray-600 font-bold">${commentary.author}</span>
                                <span class="ml-2 text-green-600 text-xs">${commentary.text}</span>
                                </p>
                                <div class="flex items-center mt-1">
                                <svg class="w-4 h-4 fill-current text-yellow-600" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M10 15l-5.878 3.09 1.123-6.545L.489 6.91l6.572-.955L10 0l2.939 5.955 6.572.955-4.756 4.635 1.123 6.545z"/></svg>
                                <svg class="w-4 h-4 fill-current text-yellow-600" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M10 15l-5.878 3.09 1.123-6.545L.489 6.91l6.572-.955L10 0l2.939 5.955 6.572.955-4.756 4.635 1.123 6.545z"/></svg>
                                <svg class="w-4 h-4 fill-current text-yellow-600" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M10 15l-5.878 3.09 1.123-6.545L.489 6.91l6.572-.955L10 0l2.939 5.955 6.572.955-4.756 4.635 1.123 6.545z"/></svg>
                                <svg class="w-4 h-4 fill-current text-yellow-600" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M10 15l-5.878 3.09 1.123-6.545L.489 6.91l6.572-.955L10 0l2.939 5.955 6.572.955-4.756 4.635 1.123 6.545z"/></svg>
                                <svg class="w-4 h-4 fill-current text-gray-400" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M10 15l-5.878 3.09 1.123-6.545L.489 6.91l6.572-.955L10 0l2.939 5.955 6.572.955-4.756 4.635 1.123 6.545z"/></svg>
                                </div>
                                `)}
                            </div>
                        </div>
                            ${this.properties.restaurant.menus.map(menu => html`
                                <div class="bg-white border overflow-hidden rounded-lg mt-2">
                                <img src=${menu.img} alt="img-pizza">
                                    <div class="p-6">
                                        <h4 class="font-semibold text-lg italic">${menu.name}</h4>
                                        <p class="text-gray-600 mt-1">${menu.description}</p>
                                        <div class="text-teal-600 font-semibold mt-2">${menu.price}€</div>
                                    </div>
                                    <div class="flex justify-end mb-2 mr-2">
                                    <button class="bg-white hover:bg-gray-100 text-gray-800 font-semibold py-2 px-4 border border-gray-400 rounded shadow" @click="${this.addToCart}" menuname="${menu.name}" menuprice="${menu.price}" menuimg="${menu.img}">
                                            Commander
                                        </button>
                                    </div>
                                </div>
                            </div>
                        `)}
                    </div>
                </section>
				`
    }

    renderView() {
        const view = this.template();
        render(view, this.page);
    }

    handleChange({ detail: restaurant }) {
        this.properties.restaurant = restaurant;
        console.log('restaurant menus', this.properties.restaurant)
        this.renderView();
    }

    handleForm(e) {
        e.preventDefault();
        const event = new CustomEvent('send-commentary', { detail: {text: this.properties.newCommentary, rating: 5 /*this.properties.newCommentaryNote*/, restaurant: this.properties.restaurant.name}});
        document.dispatchEvent(event);
        const input = document.querySelector('[name="commentaire"]');
        input.value = '';
        this.properties.newCommentary = '';
        this.properties.newCommentaryNote = 0;
    }

    addToCart(e) {
        const data = {
            name: e.srcElement.getAttribute('menuname'),
            price: e.srcElement.getAttribute('menuprice'),
            img: e.srcElement.getAttribute('menuimg')
        }
        console.log('dataaaa', data)
        let cart = localStorage.getItem("cart");
        if (cart == null) {
            cart = [data];
        } else {
            cart = JSON.parse(cart);
            cart.push(data);
        }
        localStorage.setItem("cart", JSON.stringify(cart));
        console.log('local', localStorage.getItem("cart"));
        return undefined;
    }
}
