import page from 'page';
import firebase from 'firebase/app';
import 'firebase/database';
import 'firebase/firestore';
import 'firebase/auth';

const app = document.querySelector('#app .outlet');
const skeleton = document.querySelector('#app .skeleton');

firebase.initializeApp(window.config);
const database = firebase.database();
const firestore = firebase.firestore();
const auth = firebase.auth();



auth.onAuthStateChanged(user => {
  if (!user) {
    window.localStorage.setItem('logged', 'false');
    return console.log('logged out');
  }
  window.localStorage.setItem('logged', 'true');
  window.localStorage.setItem('userId', user.uid);
  document.dispatchEvent(new CustomEvent('user-logged', { detail: user }));
});

const notificationBtn = document.querySelector('#notification');
notificationBtn.addEventListener('click', () => {
  const result = confirm('Would you like to receive push notification for more porn ?');
  if (result) {
    grantNotification();
  }
});

subcribeNotification();

//SIGNUP
page('/signup', async () => {
  // Navigation guard
  const loggedInState = window.localStorage.getItem('logged');
  if (loggedInState !== 'false') return page('/');

  const module = await import('./views/signUp.js');
  const SignUp = module.default;

  const ctn = app.querySelector('[page=signUp]');
  const SignUpView = new SignUp(ctn);

  document.addEventListener('sing-up', ({ detail: { email, password } }) => {
    alert('toto')
    auth.createUserWithEmailAndPassword(email, password)
      .then(user => {
        console.log('signup successfull', user);
        page('/restaurants');
      }).catch(console.log);
  });

  displayPage('signUp');
});

// SIGNIN
page('/signIn', async () => {
  // Navigation guard
  const loggedInState = window.localStorage.getItem('logged');
  if (loggedInState !== 'false') return page('/');

  const module = await import('./views/signIn.js');
  const SignIn = module.default;

  const ctn = app.querySelector('[page=signIn]');
  const SignInView = new SignIn(ctn);

  document.addEventListener('sing-in', ({ detail: { email, password } }) => {
    auth.signInWithEmailAndPassword(email, password)
      .then(user => {
        console.log('signIn successfull', user);
        page('/restaurants');
      }).catch(console.log);
  });

  displayPage('signIn');
});

// LOGOUT
const logout = document.querySelector('#logout');
logout.addEventListener('click', (e) => {
  e.preventDefault();
  auth.signOut();
});

page('/', async () => {
  // Navigation guard
  const loggedInState = window.localStorage.getItem('logged');
  if (loggedInState === 'false') return page('/signIn');

  const module = await import('./views/home.js');
  const Home = module.default;

  subcribeNotification();

  const messages = [];

  const ctn = app.querySelector('[page=home]');
  const HomeView = new Home(ctn);
  console.log(HomeView.properties);


  document.addEventListener('send-message', ({ detail: message }) => {
    if (!message) return;
    // Using realtime database
    // database.ref().child('/messages').push({
    //   content: message,
    //   date: Date.now()
    // });

    // Using firestore
    firestore.collection('messages').add({
      content: message,
      date: Date.now(),
      user: {
        uid: auth.currentUser.uid,
        email: auth.currentUser.email
      }
    });
  });

  displayPage('home');
});

page('/restaurants', async (ctx) => {
  const loggedInState = window.localStorage.getItem('logged');
  if (loggedInState === 'false') return page('/signIn');
  subcribeNotification();

  const messages = [];

  const module = await import('./views/restaurant.js');
  const Restaurant = module.default;
  const ctn = app.querySelector('[page=restaurant]');
  const RestaurantView = new Restaurant(ctn, ctx);
  displayPage('restaurant');
});

page('/restaurant/:id', async (ctx) => {
  const loggedInState = window.localStorage.getItem('logged');
  if (loggedInState === 'false') return page('/signIn');
  subcribeNotification();

  const messages = [];

  const module = await import('./views/menu.js');
  const Menu = module.default;
  const ctn = app.querySelector('[page=menu]');
  const MenuView = new Menu(ctn, ctx);
  document.addEventListener('send-commentary', (e) => {
    const com = e.detail;
    if (!com.text) {
        console.log("1");
        return;
    }
    if (!com.rating) {
        console.log("2");
        return;
    }
    if (!com.restaurant) {
        console.log("3");
        return;
    }
    let restaurantName = com.restaurant.toLowerCase();
    restaurantName = restaurantName.replace(/ /g, '_');
    const rest = firestore.collection('restaurants').doc(restaurantName)
    rest.get().then(restaurant => {
            const data = restaurant.data();
            const commentaries = data.commentaries;
            commentaries.push({
                author: auth.currentUser.email,
                text: com.text,
                rating: com.rating
            })
        console.log(commentaries)
            rest.update({"commentaries": commentaries});
        }
    );
});

  displayPage('menu');
});

page('/order', async (ctx) => {
  const loggedInState = window.localStorage.getItem('logged');
  if (loggedInState === 'false') return page('/signIn');
  subcribeNotification();

  const messages = [];

  const module = await import('./views/order.js');
  const Order = module.default;
  const ctn = app.querySelector('[page=order]');
  const OrderView = new Order(ctn, ctx);
  displayPage('order');
});

page();

function scrollDown() {
  setTimeout(() => {
    window.scrollTo(0, document.body.scrollHeight);
  }, 0);
}

function displayPage(name) {
  const skeleton = document.querySelector('#app .skeleton');
  skeleton.removeAttribute('hidden');
  const pages = app.querySelectorAll('[page]');
  pages.forEach(page => page.removeAttribute('active'));
  skeleton.setAttribute('hidden', 'true');
  const p = app.querySelector(`[page="${name}"]`);
  p.setAttribute('active', 'true');
}

function subcribeNotification() {
  const noficationRegistered = window.localStorage.getItem('noficationRegistered');
  if (Notification.permission === 'granted' && auth.currentUser && noficationRegistered !== 'true') {
    navigator.serviceWorker.ready
      .then(reg => {
        reg.pushManager.subscribe({
          userVisibleOnly: true,
          applicationServerKey: urlBase64ToUint8Array(window.config.publicKey)
        })
        .then(async subscription => {
          console.log(subscription);
          try {
            const result = await fetch(`${window.config.notificationBackend}/subscribe`, {
              method: 'POST',
              headers: {
                'content-type': 'application/json'
              },
              body: JSON.stringify({
                subscription,
                user: auth.currentUser.uid,
                groupe: 'all'
              })
            });
            window.localStorage.setItem('noficationRegistered', 'true');
          } catch(err) {
            console.log(err);
          }
        })
      })
  }
}

function urlBase64ToUint8Array(base64String) {
  const padding = '='.repeat((4 - base64String.length % 4) % 4);
  const base64 = (base64String + padding)
    .replace(/-/g, '+')
    .replace(/_/g, '/');

  const rawData = window.atob(base64);
  const outputArray = new Uint8Array(rawData.length);

  for (let i = 0; i < rawData.length; ++i) {
    outputArray[i] = rawData.charCodeAt(i);
  }
  return outputArray;
}





