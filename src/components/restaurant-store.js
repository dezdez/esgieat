import firebase from 'firebase/app';
import 'firebase/firestore';

export default class RestaurantStore extends HTMLElement {
    constructor() {
        super();
        this.data = [];
        this._collection = 'restaurants';
        this._document = '';
        this.mounted = false;
    }

    static get observedAttributes() {
        return ['collection', 'document'];
    }

    get collection() {
        return this._collection;
    }

    set collection(value) {
        this._collection = value;
        this.setAttribute('collection', value);
    }

    get document() {
        return this._document;
    }

    set document(value) {
        this._document = value;
        this.setAttribute('document', value);
    }

    activated() {
        if (this.mounted !== false) {
        this.dispatchEvent(new CustomEvent('data-changed', { detail: this.data }));
        }
        this.mounted = true;
    }


    /*connectedCallback() {
        this.getData();
    }*/

    attributeChangedCallback(name, oldValue, newValue) {
        console.log('change', name, oldValue, newValue)

        switch (name) {
            case 'collection':
                this._collection = newValue;
                this.getRestaurants();
                break;
            case 'document':
                this._document = newValue;
                this.getRestaurant(this._document)
                break;
        }
    }

    getRestaurant(id) {
        console.log('getting restaurant')

        const firestore = firebase.firestore();
        firestore
            .collection(this._collection)
            .doc(id)
            .onSnapshot((restaurant) => {
                console.log('get restaurant')
                const data = {
                    id: restaurant.id,
                    ...restaurant.data()
                }

                this.dispatchEvent(new CustomEvent('data-restaurant', { detail: data }));
            })
    }

    getRestaurants() {
        if (!this._collection) return;

        console.log('cocooooo', this._collection)

        const firestore = firebase.firestore();
        firestore
            .collection(this._collection)
            .onSnapshot(ref => {
                ref.docChanges().forEach(change => {
                    const { oldIndex, doc, type } = change;

                    if (type === 'added') {
                        // this.data = [...this.data, doc.data()];

                        const data = {
                            id: doc.id,
                            ...doc.data()
                        }

                        this.data = [...this.data, data];

                        console.log('dataaaa', this.data)

                        // this.data[i++].id = change.newIndex;
                        this.dispatchEvent(new CustomEvent('data-changed', { detail: this.data }));
                    } else if (type === 'removed') {
                        this.data.splice(oldIndex, 1);
                        this.dispatchEvent(new CustomEvent('data-changed', { detail: this.data }));
                    }
                })
            });
    }
}

customElements.define('restaurant-store', RestaurantStore);
